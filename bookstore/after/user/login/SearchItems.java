package bookstore.after.user.login;

import java.awt.EventQueue;


import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

public class SearchItems {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SearchItems window = new SearchItems();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SearchItems() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblSearchForSoftwares = new JLabel("Which Item Do You Want To Buy");
		lblSearchForSoftwares.setBounds(20, 19, 202, 16);
		frame.getContentPane().add(lblSearchForSoftwares);
		
		JButton btnBooks = new JButton("Books");
		btnBooks.setBounds(161, 102, 117, 29);
		frame.getContentPane().add(btnBooks);
		
		JButton btnBuyNow = new JButton("CD");
		btnBuyNow.setBounds(161, 156, 117, 29);
		frame.getContentPane().add(btnBuyNow);
		
		JButton btnSoftware = new JButton("Software");
		btnSoftware.setBounds(161, 216, 117, 29);
		frame.getContentPane().add(btnSoftware);
	}
}
