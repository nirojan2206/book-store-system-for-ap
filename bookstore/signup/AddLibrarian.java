
package bookstore.signup;
import java.awt.EventQueue;


import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class AddLibrarian {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddLibrarian window = new AddLibrarian();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AddLibrarian() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblLibrarianAddingForm = new JLabel("Librarian Adding Form");
		lblLibrarianAddingForm.setBounds(6, 6, 181, 16);
		frame.getContentPane().add(lblLibrarianAddingForm);
		
		JLabel lblUserName = new JLabel("First Name:");
		lblUserName.setBounds(56, 52, 81, 16);
		frame.getContentPane().add(lblUserName);
		
		JLabel lblPassword = new JLabel("Last Name:");
		lblPassword.setBounds(56, 80, 81, 16);
		frame.getContentPane().add(lblPassword);
		
		textField = new JTextField();
		textField.setBounds(174, 47, 141, 26);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(173, 80, 142, 26);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblPassword_1 = new JLabel("Password:");
		lblPassword_1.setBounds(66, 118, 70, 16);
		frame.getContentPane().add(lblPassword_1);
		
		textField_2 = new JTextField();
		textField_2.setBounds(174, 113, 141, 26);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
	}
}
