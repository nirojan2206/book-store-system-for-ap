package bookstore.login;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JButton;

public class UserLogin {

	private JFrame frame;
	private JTextField tbUserLogin;
	private JPasswordField passUserLogin;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserLogin window = new UserLogin();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public UserLogin() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		tbUserLogin = new JTextField();
		tbUserLogin.setBounds(178, 60, 130, 26);
		frame.getContentPane().add(tbUserLogin);
		tbUserLogin.setColumns(10);
		
		passUserLogin = new JPasswordField();
		passUserLogin.setBounds(178, 129, 130, 26);
		frame.getContentPane().add(passUserLogin);
		
		JLabel lblNewLabel = new JLabel("User Name:");
		lblNewLabel.setBounds(71, 65, 95, 16);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setBounds(88, 134, 78, 16);
		frame.getContentPane().add(lblPassword);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.setBounds(90, 196, 117, 29);
		frame.getContentPane().add(btnLogin);
		
		JLabel lblUserLoginPage = new JLabel("User Login Page");
		lblUserLoginPage.setBounds(19, 17, 163, 26);
		frame.getContentPane().add(lblUserLoginPage);
		
		JButton btnSignUp = new JButton("Sign Up");
		btnSignUp.setBounds(255, 196, 117, 29);
		frame.getContentPane().add(btnSignUp);
	}
}
